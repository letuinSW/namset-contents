import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Scanner;

public class Main {
  private static int countLeaf(LeafBranchesMap leafBranchesMap, int startLeaf, Branch[] cutBranches) {
    var countedLeaves = new HashSet<Integer>(Arrays.asList(startLeaf));
    var searchingLeaves = new ArrayList<Integer>(Arrays.asList(startLeaf));

    while (!searchingLeaves.isEmpty()) {
      var leaf = searchingLeaves.remove(searchingLeaves.size() - 1);
      var branches = leafBranchesMap.getBranches(leaf);

      for (var branch : branches) {
        if (Arrays.stream(cutBranches).anyMatch(branch::equals)) {
          continue;
        }

        var oppositeLeaf = branch.GetOppositeLeaf(leaf);
        if (countedLeaves.contains(oppositeLeaf)) {
          continue;
        }

        countedLeaves.add(oppositeLeaf);
        searchingLeaves.add(oppositeLeaf);
      }
    }

    return countedLeaves.size();
  }

  private static boolean checkDividingIn3EqualParts(LeafBranchesMap leafBranchesMap, Branch[] cutBranches) {
    var startLeaves = new ArrayList<Integer>();
    for (var cutBranch : cutBranches) {
      var leaves = cutBranch.GetLeaves();
      for (var leaf : leaves) {
        startLeaves.add(leaf);
      }
    }

    var leafCount = countLeaf(leafBranchesMap, startLeaves.get(0), cutBranches);
    for (var leaf : startLeaves.subList(1, startLeaves.size())) {
      if (leafCount != countLeaf(leafBranchesMap, leaf, cutBranches)) {
        return false;
      }
    }
    return true;
  }

  public static void main(String[] args) {
    int n;
    Branch[] branches;
    var leafBranchesMap = new LeafBranchesMap();

    try (var scanner = new Scanner(System.in)) {
      n = scanner.nextInt();
      branches = new Branch[n - 1];
      for (var i = 0; i < n - 1; i += 1) {
        var leaf1 = scanner.nextInt();
        var leaf2 = scanner.nextInt();

        var branch = new Branch(leaf1, leaf2);
        branches[i] = branch;

        leafBranchesMap.addBranch(leaf1, branch);
        leafBranchesMap.addBranch(leaf2, branch);
      }
    }

    for (var x = 0; x < n - 1; x += 1) {
      for (var y = x + 1; y < n - 1; y += 1) {
        var cutBranches = new Branch[] { branches[x], branches[y] };
        var isDividedIn3EqualParts = checkDividingIn3EqualParts(leafBranchesMap, cutBranches);
        if (isDividedIn3EqualParts) {
          System.out.print(String.format("%d %d", x, y));
          return;
        }
      }
    }

    System.out.print("-1");
  }
}
#include <iostream>
#include <vector>
#include <map>
#include <set>
#include <queue>

struct Branch
{
    int index;
    int leaf1;
    int leaf2;
};

class LeafBranchesMap
{
public:
    std::vector<Branch> getBranches(int leaf)
    {
        if (_leafBranchesMap.find(leaf) == _leafBranchesMap.end())
        {
            _leafBranchesMap[leaf] = std::vector<Branch>();
        }

        return _leafBranchesMap[leaf];
    }

    void addBranch(int leaf, Branch branch)
    {
        if (_leafBranchesMap.find(leaf) == _leafBranchesMap.end())
        {
            _leafBranchesMap[leaf] = std::vector<Branch>();
        }

        _leafBranchesMap[leaf].push_back(branch);
    }

private:
    std::map<int, std::vector<Branch>> _leafBranchesMap;
};

int countLeaves(LeafBranchesMap &leafBranchMap, int startLeaf, int cutBranchIndex1, int cutBranchIndex2)
{
    std::set<int> countedLeaves;
    std::queue<int> nextLeaves;
    nextLeaves.push(startLeaf);

    while (!nextLeaves.empty())
    {
        auto leaf = nextLeaves.front();
        nextLeaves.pop();

        if (countedLeaves.find(leaf) != countedLeaves.end())
        {
            continue;
        }
        countedLeaves.insert(leaf);

        auto branches = leafBranchMap.getBranches(leaf);
        for (auto &branch : branches)
        {
            if (branch.index == cutBranchIndex1 || branch.index == cutBranchIndex2) {
                continue;
            }
            
            if (countedLeaves.find(branch.leaf1) == countedLeaves.end())
            {
                nextLeaves.push(branch.leaf1);
            }
            if (countedLeaves.find(branch.leaf2) == countedLeaves.end())
            {
                nextLeaves.push(branch.leaf2);
            }
        }
    }

    return countedLeaves.size();
}

bool isDividedInto3(std::vector<Branch> &branches, LeafBranchesMap &leafBranchesMap, int cutBranchIndex1, int cutBranchIndex2)
{
    auto branch1 = branches[cutBranchIndex1];
    auto branch2 = branches[cutBranchIndex2];

    auto count = countLeaves(leafBranchesMap, branch1.leaf1, cutBranchIndex1, cutBranchIndex2);

    return count == countLeaves(leafBranchesMap, branch1.leaf2, cutBranchIndex1, cutBranchIndex2) 
        && count == countLeaves(leafBranchesMap, branch2.leaf1, cutBranchIndex1, cutBranchIndex2) 
        && count == countLeaves(leafBranchesMap, branch2.leaf2, cutBranchIndex1, cutBranchIndex2);
}

int main()
{
    std::vector<Branch> branches;
    LeafBranchesMap leafBranchesMap;
    int n;
    std::cin >> n;

    for (auto i = 0; i < n - 1; i += 1)
    {
        Branch branch;
        branch.index = i;
        std::cin >> branch.leaf1 >> branch.leaf2;

        branches.push_back(branch);
        leafBranchesMap.addBranch(branch.leaf1, branch);
        leafBranchesMap.addBranch(branch.leaf2, branch);
    }

    for (auto cutBranchIndex1 = 0; cutBranchIndex1 < branches.size(); cutBranchIndex1 += 1)
    {
        for (auto cutBranchIndex2 = cutBranchIndex1 + 1; cutBranchIndex2 < branches.size(); cutBranchIndex2 += 1)
        {
            if (isDividedInto3(branches, leafBranchesMap, cutBranchIndex1, cutBranchIndex2))
            {
                std::cout << cutBranchIndex1 << " " << cutBranchIndex2 << std::endl;
                return 0;
            }
        }
    }

    std::cout << "-1" << std::endl;
    return 0;
}
const fs = require('fs');

const filePath = process.argv[2];
const fileString = fs.readFileSync(filePath, 'utf-8');
const lines = fileString.split(/\r?\n/)

class Branch {
    constructor(leaf1, leaf2) {
        this.leaf1 = leaf1;
        this.leaf2 = leaf2;
    }
}

function countLeaf(leafBranchesMap, startLeaf, cutBranches) {
    const countedLeaves = new Set([startLeaf]);
    const searchingLeaves = [startLeaf];

    while (searchingLeaves.length) {
        const leaf = searchingLeaves.pop();
        const connectedBranches = leafBranchesMap[leaf];

        for (const branch of connectedBranches) {
            if (cutBranches.includes(branch)) {
                continue;
            }
            const oppositeLeaf = branch.leaf1 == leaf ? branch.leaf2 : branch.leaf1;
            if (countedLeaves.has(oppositeLeaf)) {
                continue;
            }
            countedLeaves.add(oppositeLeaf);
            searchingLeaves.push(oppositeLeaf);
        }
    }
    return countedLeaves.size;
}

function checkDividingIn3EqualParts(leafBranchesMap, cutBranches) {
    const startLeaves = [];
    cutBranches.forEach(branch => {
        startLeaves.push(branch.leaf1, branch.leaf2);
    });
    const leafCount = countLeaf(leafBranchesMap, startLeaves[0], cutBranches);
    for (const startLeaf of startLeaves) {
        if (leafCount != countLeaf(leafBranchesMap, startLeaf, cutBranches)) {
            return false;
        }
    }
    return true;
}

function AddBranchInLeafBranchesMap(leafBranchesMap, leaf, branch) {
    if (!leafBranchesMap[leaf]) {
        leafBranchesMap[leaf] = [];
    }
    leafBranchesMap[leaf].push(branch);
}

const n = parseInt(lines[0]);
const leafBranchesMap = {};
const branches = [];

for (let branchIndex = 0; branchIndex < n - 1; branchIndex += 1) {
    const [leaf1, leaf2] = lines[branchIndex + 1].split(' ').map(x => parseInt(x));
    const branch = new Branch(leaf1, leaf2);
    AddBranchInLeafBranchesMap(leafBranchesMap, leaf1, branch);
    AddBranchInLeafBranchesMap(leafBranchesMap, leaf2, branch);
    branches.push(branch);
}

for (let branchXIndex = 0; branchXIndex < n - 1; branchXIndex += 1) {
    for (let branchYIndex = branchXIndex + 1; branchYIndex < n - 1; branchYIndex += 1) {
        const cutBranches = [branches[branchXIndex], branches[branchYIndex]];
        const isDividedIn3EqualParts = checkDividingIn3EqualParts(leafBranchesMap, cutBranches)
        if (isDividedIn3EqualParts) {
            console.log(`${branchXIndex} ${branchYIndex}`);
            return;
        }
    }
}

console.log('-1');
return;
public class Branch {
    private int leaf1;
    private int leaf2;

    public Branch(int leaf1, int leaf2) {
        this.leaf1 = leaf1;
        this.leaf2 = leaf2;
    }

    public int GetOppositeLeaf(int leaf) {
        return leaf == this.leaf1 ? this.leaf2 : this.leaf1;
    }

    public int[] GetLeaves() {
        return new int[] { leaf1, leaf2 };
    }
}

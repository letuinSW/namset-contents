class Branch:
    def __init__(self, leaf1, leaf2):
        self.leaf1 = leaf1
        self.leaf2 = leaf2
    def __repr__(self):
        return f'{self.leaf1} {self.leaf2}'

def countLeaves(leafBranchesMap, startLeaf, cutedBranchA, cutedBranchB):
    countedLeaves = set([startLeaf])
    searchingLeaves = [startLeaf]
    while len(searchingLeaves) > 0:
        leaf = searchingLeaves.pop()
        connectedBranches = leafBranchesMap[leaf]
        for branch in connectedBranches:
            if branch in [cutedBranchA, cutedBranchB]:
                continue
            oppositeSideLeaf = branch.leaf1 if leaf == branch.leaf2 else branch.leaf2
            if oppositeSideLeaf in countedLeaves:
                continue
            countedLeaves.add(oppositeSideLeaf)
            searchingLeaves.append(oppositeSideLeaf)

    return len(countedLeaves)

def checkDividingIn3EqualParts(leafBranchesMap, cutedBranchA, cutedBranchB):
    leafCount = countLeaves(leafBranchesMap, cutedBranchA.leaf1, cutedBranchA, cutedBranchB)
    return leafCount == countLeaves(leafBranchesMap, cutedBranchA.leaf2, cutedBranchA, cutedBranchB)\
        and leafCount == countLeaves(leafBranchesMap, cutedBranchB.leaf1, cutedBranchA, cutedBranchB)\
        and leafCount == countLeaves(leafBranchesMap, cutedBranchB.leaf2, cutedBranchA, cutedBranchB)

n = int(input())
branches = []
leafBranchesMap = {}

def addToLeafBranchesMap(leafBranchesMap, leaf, branch):
    if leafBranchesMap.get(leaf) is None:
        leafBranchesMap[leaf] = []
    leafBranchesMap[leaf].append(branch)

for i in range(0, n - 1):
    leaf1, leaf2 = map(int, input().split())
    branch = Branch(leaf1, leaf2)
    branches.append(branch)
    addToLeafBranchesMap(leafBranchesMap, leaf1, branch)
    addToLeafBranchesMap(leafBranchesMap, leaf2, branch)

for x in range(0, n - 1):
    for y in range(x + 1, n - 1):
        branchX = branches[x]
        branchY = branches[y]
        isDividedIn3EqualParts = checkDividingIn3EqualParts(leafBranchesMap, branchX, branchY)
        if isDividedIn3EqualParts:
            print(f'{x} {y}')
            exit()

print('-1')
exit()
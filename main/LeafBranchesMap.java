import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class LeafBranchesMap {
  private final Map<Integer, ArrayList<Branch>> innerMap = new HashMap<Integer,ArrayList<Branch>>();
  
  public void addBranch(int leaf, Branch branch) {
    var branches = innerMap.get(leaf);
    if (branches == null) {
      branches = new ArrayList<Branch>();
      innerMap.put(leaf, branches);
    }

    branches.add(branch);
  }

  public Iterable<Branch> getBranches(int leaf) {
    return innerMap.get(leaf);
  }
}

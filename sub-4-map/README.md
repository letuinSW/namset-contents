# 무엇을 연습해볼까?
여러분, 똑같은 것을 나라마다 다르게 부르는 경우가 있죠?
한국어로 차, 일본어로 구루마, 영어로는 카, 중국어는... 발음을 못하겠어요.

마찬가지로 똑같은 자료구조를 프로그래밍 언어별로 다르게 부르는 경우가 있습니다.
이번에 배울 Map, Dictionary, Hash, HashTable, Key-Value Database 가 바로 그렇습니다.

이 모든걸 어우르는 말이 있는데요, Associative Array 어소시에이티브 어레이, 즉 연관배열 입니다.

이 친구들이 뭐하는 자료구조냐면, key와 value를 저장하는 자료구조입니다.
보통 우리가 배열이든 리스트든, 값들을 저장하죠?
하지만 여기는 값과 함께 그것을 가리키는 key를 저장합니다.
그래서 나중에 key를 가지고 값을 가져올 수 있어요.

예를 들어, 수납정리함 아세요? 각 칸마다 이름을 쓸 수 있는 수납정리함이요.
![수납정리함](http://img.danawa.com/prod_img/500000/476/124/img/7124476_1.jpg?shrink=360:360&_v=20200330132717)

여기 앞에다 쓰는게 key고, 이 안에 들어있는게 value라고 생각하면 돼요.
만약 제가 약들을 이 수납정리함에 저장한다고 하면, 앞 종이에 "아스피린"이라고 써놓고, 안에 아스피린을 넣어놓을 수 있어요. 그 다음에 제가 아스피린이 필요하면 일일이 다 안꺼내봐도 앞에 아스피린 써져있는것만 보고 열어서 쓰면 돼요.

다시 정리하면, Map, Dictionary, Hash, HashTable, Key-Value 이런 녀석은 Key와 Value를 저장하는 공간인데, 언어마다 이름이 다를 뿐이고, Key를 가지고 Value를 가져올 수 있는 자료구조이다~ 라고 이해하면 돼요.

이제부턴 우리는 Map이라고 부를게요!

아! 참고로 C언어는 기본적으로 Map이나 Dictionary, Hash같은게 없어요... C++은 있어요.

# 약 저장하기

말이 나온김에, 약 재고를 저장하는 프로그램을 만들어볼까요?
이 프로그램은 입력으로 약 이름을 하나씩 받고요, 약 별로 갯수를 차곡차곡 세서, 제가 물어보는 약이 몇개인지 출력해주는 프로그램이에요.

예를들어, 입력이 '아스피린, 아스피린, 아세트아미노펜, 덱시부프로펜, 아세트아미노펜, 이소트레티노인' 이렇게 들어온다고 해봅시다.
그러면 아스피린은 2개고, 아세트아미노펜도 2개, 덱시부프로펜 1개, 이소트레티노인 1개네요. 그렇죠?
제가 '아스피린 몇개에요?' 하면 2개, '이소트레티노인 몇개에요?' 하면 1개라고 출력하는 프로그램을 만들어야해요.

이 때, 우리는 Map에 key를 약 이름으로, value를 갯수로 저장해볼거에요.

Map의 변수 이름은 {key}{value}Map 이라고 많이들 지어요. 저같으면 '약갯수Map', 영어로 'medicineCountMap'이라고 해볼게요.

아스피린이 들어오면, 우리는 medicineCountMap에 key가 아스피린인 value를 1을 올려줘야해요.
어떤 언어는 마치 배열처럼 대괄호[]을 써서, 그 안에 key를 넣어요. medicineCountMap['아스피린'] 같이요. 그러면 value가 나와요. 배열이랑 똑같죠? 배열은 숫자가 들어가는거고, 여기는 key가 들어가는 것 뿐이에요.
그러면 value를 1 올리는건 어떻게 할까요? medicineCountMap['아스피린'] += 1 하면 되겠죠?
아마 안될거에요. 왜? 처음엔 medicineCountMap['아스피린'] 에 아무것도 안들어있거든요. 0이 아니에요. null이에요. 아무것도 없어요. '아무것도 없는 것 + 1'은 에러에요.

그래서 일단 확인을 해야합니다, Map님, '아스피린' key에 해당하는 value가 있으신지요?
보통 has라는 함수같은게 있어요. medicineCountMap.has('아스피린') 하면 boolean 값으로 있다 없다를 알려줘요.
아니면 그냥 medicineCountMap['아스피린'] 이 null이 나오는 언어도 있어요. 이런 경우에는 null check를 하면 되겠죠?

자 그러면 이제 한번 코드를 짜보도록 할게요.

(코드는 [./medicineCountMap](./medicineCountMap)를 참고해주세요)

# Map의 속도는 어떻게 되나요? (시간복잡도)
여러분, 자료구조에서 가장 중요한 것은 "속도"입니다.
우리가 이 자료구조로 뭔가를 하려고 할 때 속도가 얼마나 나오는지를 알아야합니다.

우리가 자료구조로 할 수 있는게 무엇이 있습니까? 보통 '저장'과 '불러오기'입니다.
이 자료구조는 데이터를 저장하는데 얼마나 걸립니까?
이 자료구조는 데이터를 가져오는데 얼마나 걸립니까?
이게 자료구조에서 가장 중요한 부분입니다.

우리는 약의 갯수를 세기 위해 Map을 사용했습니다. 그러면 Map에 약 갯수를 저장하는덴 얼마나 시간이 걸리고, 약 갯수를 가져오는덴 얼마나 시간이 걸릴까요?

음...... 사실 정답은 "구현을 어떻게 했느냐에 따라 다릅니다"입니다.
무슨 말입니까?
Map은 추상적인 이름입니다. Map은 그 자체로는 아무것도 아니구, 그냥 특징을 나타내는 것일 뿐이에요.
무슨 말인지 이해가 안되니, 비유를 해보겠습니다.

여러분, 공은 어떤 물체입니까? 글쎄요, 공의 종류, 공의 재료 등에 따라 다르지 않을까요?
당구공은 딱딱하고요, 축구공은 탱탱하고요, 그보다 농구공이 더 탱탱하고, 미식축구공은 삐쭉해요.
하지만 모두 다 '공'이에요. '공'은 굴러가요. '구'에 가까운 모습이에요(미식축구공은 좀 다르네요?).

마찬가지로, Map도 Map일 뿐입니다. Map은 Key로 Value를 저장하고, Key로 Value를 가져올 수 있어요.
하지만 내부에서 어떤식으로 저장할지는? 어떻게 만드느냐에 따라 달려있어요.

Java에는 HashMap이라는 것이 있습니다. Hash라는 알고리즘을 이용해서 만든 Map이에요.
Hash란 뭘까요? 안알려줄거에요. 너무 길어요. 대신, 저는 HashMap의 특징만 알려줄거에요.
- HashMap은 Key를 이용해서 Value를 저장하는데 보통 O(1), 상수시간만큼 걸려요
- HashMap은 Key를 이용해서 Value를 가져오는데 보통 O(1), 상수시간만큼 걸려요

잉, '상수시간'이랑 'O(1)'은 뭔데요?
우리가 보통 '상수시간이 걸린다' 혹은 '시간복잡도가 O(1), 빅-오-오브-1이다'라고 하는 것은 아무리 HashMap에 많은 데이터를 저장해도, 그 갯수가 새로운 것을 저장하거나 가져오는데 영향을 미치지 않는다는 것을 의미해요.
HashMap에 아무것도 안들어있든, 100만개 들어있든, 1개 넣는데 속도에 변함이 없다는거에요. 빠르다는거겠죠?

음... 다른 경우는 뭐가 있을까요, 상수시간이 아닌 것들도 있어요. '데이터를 복사하는 행위'는 얼마나 걸릴까요? 보통 'O(N)'일거에요. N을 데이터의 사이즈라고 했을 때, 데이터의 크기만큼 데이터를 복사하는게 느려질거에요. 50kb 복사하는게 100mb 복사하는 것보다 빠르잖아요? 이런 애들은 상수시간이 아닌거죠. 크기와 비례해서 늘어나니 빅-오-오브-N 인거죠.

더 자세한건 알고리즘 시간에 배우도록 하구요! '시간복잡도'라고 구글에 검색하면 공부할 수 있을거에요.

자. 그래요. 그러면 Java는 HashMap을 사용하면 저장된 용량에 관계없이 항상 빠르게 접근할 수 있네요? 그러면 Python은요? Javascript는요? C++은요?
모르겠어요.
엥?
모르겠어요...
그렇게 무책임하면 어떡해요??
모르겠는걸 어떡해요 ㅎㅎㅎㅎㅎㅎ

대신, 너무 궁금하다 싶으면 다음과 같이 구글링해보세요.
'Python Dictionary Time Complexity'
'Javascript Map Time Complexity'
'C++ Map Time Complexity'

언어 + Map + Time Complexity 라고 검색하면 그 언어의 Map의 시간복잡도를 알 수 있어요.
(Map 말고 다른 자료구조에 대해 궁금하다면, {언어} + {자료구조 이름} + 'Time Complexity'라고 검색하면 되겠네요? ㅎㅎ)

대충 보아하니, Python은 Hash 알고리즘을 사용하나봐요. 그럼 O(1)이겠네요. 물론, Python 구현체마다 다를수도 있어요. 표준에 Hash 알고리즘 쓰라고 되어있으면 모르겠는데... ㅎㅎ
Javascript는 v8엔진(nodejs나 크롬)에서는 Hash를 사용한대요. 이것도 O(1). 모질라의 Firefox는 어떨까요?
C++는 의외로 Map이 Hash를 사용하지 않고 red-black tree를 이용한대요. 그래서 O(log(n))이라고 하네요. 10개가 들어있으면 2배, 1000개가 들어있으면 10배 느려지겠네요.

**왜 이걸 알아야할까요?**
대부분의 코딩테스트는 입력되는 데이터를 처리해야하는 연산 시간이 정해져있어요. 보통 1초로 되어있죠?
만약 우리가 선택한 자료구조가 입력된 데이터가 많을수록 너무 느려진다면 어떻게 될까요?
시간 초과로 0점이 되는거에요. 울랄라!

그러므로, 여러분들은 기본적인 자료구조들의 시간복잡도를 알고 계셔야합니다.
그래서 시간복잡도를 기준으로 "음.. 시간 초과가 될 것 같은데? 아닌데?"를 가늠하실 수 있어야합니다.

잘 모르겠다고요?
나중에 천천히 시간복잡도에 대해 공부해보시면 됩니다.
공부 안하면 안되냐구요?
ㅎㅎ 안해도 괜찮아요.
근데 저는 가급적 시간복잡도에 대해 알고 있는 분과 같이 일을 하고 싶어요. 그래야 우리 프로그램의 예상 연산속도를 알 수 있고, 최적화도 할 수 있으니깐요.
상상해보세요, 시간복잡도라는걸 공부하면 우리의 프로그램이 더 빠르고 멋지게 돌아가는데 도움이 될거에요! 이러면 배우고 싶지 않나?ㅎㅎㅎㅎ

# 문제 하나 더 풀어보기 : 서로이웃 부탁드려요~!
네X버 블로그에 보면 서로이웃이라는 기능이 있습니다. 인X타나 트위X의 팔로우같은 건가봐요.

프로그램의 입력으로 '누가 누구랑 서로이웃인지'에 대한 정보가 들어오고, '저는 사람 X에 대해 궁금해요!'라고 입력이 들어오면, 사람 X와 이웃인 사람, 그리고 이웃의 이웃인 사람들을 출력해주는 프로그램을 만들어볼까요?

처음에 입력으로 숫자 N이 주어집니다. 이 N은 앞으로 '누가 누구랑 서로이웃인지'에 대한 정보가 N개 들어온다는 것을 의미합니다.

그 다음에 N개의 줄에 걸쳐서, 사람 이름 2개가 주어집니다. 아래와 같이요!

```
6
임재원 남세현
임재원 김성태
김성태 김진효
한동숙 김진효
전상빈 김진효
노채리 김성태
남세현 전상빈
```

이름이 어디서 많이 본 것 같은 학생은, 착각일 수 있습니다.

위 입력에서 '임재원'의 이웃은 누구인가요? '임재원'의 이웃은 ['남세현', '김성태'] 입니다. 2명입니다. 즉, 사람 X의 이웃은 0명일수도, 1명일수도, 혹은 그 이상일수도 있습니다.

## 1. 데이터 입력받아 저장하기

그렇다면 우리는 데이터를 어떻게 저장해야할까요?
한 사람 X에 대해서, 다수의 이웃을 저장할 수 있으면 좋을 것 같아요.
우리가 지금 Map, Dictionary에 대해서 공부하고 있으니, Map으로 저장한다면 어떻게 저장하는게 좋을까요?

저라면 이렇게 저장을 할 것 같아요,
Map의 Key는 사람 이름!
그리고 Map의 Value는 사람들의 이름! '사람 이름 배열' 혹은 '사람 이름 리스트'인거죠.

이름은 문자열이니 string이라고 하면,
key는 string
value는 string[] 혹은 List<string>일거에요. (아마 타입을 잘 안다루는 언어인 파이썬이나 자바스크립트를 하는 학생들은 무슨 말인지 약간 어려울수도?)

그렇다면, Map<string, string[]>에 어떻게 데이터를 저장하면 될까요?
사람 a, b가 들어오면 A의 이웃 리스트에 B를 추가하고, B의 이웃 리스트에 A를 추가하면 됩니다.

```python
neighborsMap = {} # key: string, value: string[]

a = '임재원'
b = '김성태'

neighborsMap[a].append(b)
neighborsMap[b].append(a)
```

아마 위와 같은 코드를 돌리면 에러가 날겁니다.
왜지!? 왜냐하면, 맵 neighborsMap에는 사람 A에 대한 리스트가 저장이 안되어있어요. Map[a]는 None이에요(null이에요).
그래서 저장을 하기에 앞서, 'A에 대한 이웃 리스트가 있는지 체크를 하고, 없으면 리스트 먼저 만들어주기' 작업이 필요해요. 마치 다음과 같이요.

```python
if neighborsMap[a] is None:
    neighborsMap[a] = []

neighborsMap[a].append(b)
```

이거는 파이썬만 그런게 아니에요. 여러분들이 사용하시는 모든 언어가 다 똑같습니다. 그래서 여러분들이 파이썬 코드를 보고 "어 나는 파이썬 안하는데... 나 모르겠는데" 하실 필요가 없습니다. 잘 보면 다 똑같아요.

그래서 이렇게 Map의 Value가 없는지 체크하고, 없으면 List를 만들어 넣어준다면, 아마 다음과 같이 코드가 완성됩니다.

```python
neighborsMap = {} # key: string, value: string[]

a = '임재원'
b = '김성태'

if neighborsMap.get(a) is None:
    neighborsMap[a] = []
neighborsMap[a].append(b)

if neighborsMap.get(b) is None:
    neighborsMap[b] = []
neighborsMap[b].append(a)
```

근데 여러분, 잘 보니 뭔가 똑같은 일을 'a'와 'b'만 바꿔서 하고 있는 것 같지 않나요?
이 아래 코드를 뭔가... 함수따위로 묶을 수 있지 않을까요?
```python
if neighborsMap.get(a) is None:
    neighborsMap[a] = []
neighborsMap[a].append(b)
```

위 코드를 잘보니, 이런 일을 하는 것 같아요.
1. 서로이웃리스트Map에 사람 A에 해당하는 서로이웃리스트가 있는지 보기
2. 없으면 A를 위한 서로이웃리스트를 새로 만들어 넣어주기
3. 사람 A의 서로이웃리스트에 사람 B를 추가하기

그렇다면 저는 이 작업을 한마디로 '서로이웃 리스트에 넣어주기'라고 요약할 수 있을 것 같아요.
그럼 그걸 함수 이름으로 지으면 어떨가요? 'addInNeighbors' 정도로요. 함수 인자로는 사람 A와 B의 이름을 받으면 될 것 같아요. 다음과 같이 해볼까요?

```python
def addInNeighbors(neighborsMap, user, neighbor):
    if neighborsMap.get(user) is None:
        neighborsMap[user] = []
    neighborsMap[user].append(neighbor)
```

그러면 이제 우리는 편하게 `addInNeighbors(neighborsMap, a, b)`, `addInNeighbors(neighborsMap, b, a)`만 열심히 호출하면 되겠네요? 그렇습니다. N개 줄에 사람들이 막 들어오면 위 함수만 열심히 호출하면 neighborsMap에 저장이 될 것입니다.

```python
def addInNeighbors(neighborsMap, user, neighbor):
    if neighborsMap.get(user) is None:
        neighborsMap[user] = []
    neighborsMap[user].append(neighbor)

neighborsMap = {} # key: string, value: string[]

N = int(input())
for _ in range(N):
    a, b = input().split()
    addInNeighbors(neighborsMap, a, b)
    addInNeighbors(neighborsMap, b, a)
```


그 다음엔 무엇을 해야할까요?

## 2. 데이터 입력받아 가져오기

우리는 이제 사람 x에 대해 궁금하다고 입력이 들어오면, 그 사람 x와 이웃인 사람, 그리고 이웃의 이웃인 사람들을 출력해야합니다.

x와 이웃인 사람은 어떻게 가져올 수 있을까요?
이제까지 우리가 열심히 저장했던 neighborsMap에서 가져오면 될 것 같아요. key를 사람 X로 하면 그 X의 이웃들이 Value로 주어지겠죠? 맞습니다.

그렇다면 '이웃의 이웃'은 어떻게 가져오면 될까요?
앞서 가져온 이웃 리스트에서 한명씩 뽑아, 또 neighborsMap에서 가져오면 됩니다. 그리고 합치면 돼요. 의외로 생각보다 
쉽습니다. 그쵸?

그럼 이렇게 해봅시다. '사람 user의 이웃 리스트를 가져오는 함수'를 만듭시다.

한번 코드로 볼게요.

```cpp
std::vector<std::string> getNeighbors(NeighborsMap& neighborsMap, std::string user) {
    if (neighborsMap.find(user) == neighborsMap.end()) {
        // x의 이웃은 없는뎁쇼..?
        return std::vector<std::string>(); // 빈 리스트 내보내기.
    }
    return neighborsMap[user];
}
```

그 다음, '사람 user의 이웃과, 이웃의 이웃을 가져오는 함수'를 만들어봅시다.
일단 user의 이웃 가져오고요, 가져온 리스트에서 for루프를 돌며 또 이웃을 가져오면 됩니다.

```cpp
std::vector<std::string> getNeighborAndNeighborsOfNeighbor(NeighborsMap& neighborsMap, std::string user) {
    std::vector<std::string> result;

    auto neighbors = getNeighbors(neighborsMap, user);
    result.insert(result.end(), neighbors.begin(), neighbors.end());
    
    for (auto& neighbor : neighbors) {
        auto neighborsOfNeighbors = getNeighbors(neighborsMap, neighbor);
        result.insert(result.end(), neighborsOfNeighbors.begin(), neighborsOfNeighbors.end());
    }

    return result;
}
```

어라라? 근데 이거, 실제로 돌려보면 뭔가 이상한데요?
뭐가 이상하냐면, 중복된 사람들이 또 나와요.
a와 b와 c가 서로서로 서로이웃이라고 합시다.
```
a b
b c
a c
```
인 관계에요.

a를 지목해봅시다. a의 서로이웃은 b와 c에요. 그럼 결과 리스트엔 [b, c]가 저장될거에요.
그 다음, b의 서로이웃은 a, c이고, c의 서로이웃은 a, b에요. 그걸 다 결과리스트에 넣으면 [b, c, a, c, a, b]가 돼요.
어라라;; 지금 무엇이 문제인가요? 중복된 데이터가 들어가는게 문제네요. [b, c, a, c, a, b]를 보면 b가 2개, c도 2개, a도 2개에요.

이미 들어간 사람이면 넣지 말아야해요.

어떻게 하면 이 문제를 해결할 수 있을까요?
저는 2가지 정도의 답을 알고 있어요.

### 1. 중복 데이터 제거를 위해 for문 열심히 돌리기
대부분의 인간이 이런식으로 중복 데이터를 제거하는데요,
여러분 앞에 무작위 카드 더미가 있다고 합시다. 여기서 중복된 카드는 제거하고 싶어요.
이런 경우 어떻게 하시나요?

저라면 일단 카드 더미를 오른쪽쪽에 쌓아놓고, 한장씩 가져옵니다.
그 다음에, 왼쪽에 카드를 한장씩 펼쳐놓습니다.
오른쪽에서 카드 한장 뽑을 때, 왼쪽에 그 카드가 있는지 아닌지를 봅니다.
없으면 추가하고요, 이미 왼쪽에 있으면 추가하지 않습니다.

지금 제가 하는 행동을 그대로 코드로 짜면 다음과 같을 거에요.

```javascript
var 왼쪽카드더미 = [];
var 오른쪽카드더미 = 카드더미;

for (var 오른쪽카드 in 오른쪽카드더미) {
    var 오른쪽_카드는_왼쪽_카드_더미에_있었습니까 = false;

    for (var 왼쪽카드 in 왼쪽카드더미) {
        if (왼쪽카드 == 오른쪽카드) {
            오른쪽_카드는_왼쪽_카드_더미에_있었습니까 = true
            break;
        }
    }

    if (오른쪽_카드는_왼쪽_카드_더미에_있었습니까 == false) {
        왼쪽카드더미.추가(오른쪽카드)
    }
}

print('카드의 종류는 => ', 왼쪽카드더미);
```

이렇게 하면 대충 N * N번의 시간이 걸리겠네요? 시간복잡도가 빅-오-오브-N-제곱, O(N^2) 걸릴 것 같아요.

만약 카드가 너~무 많다면 이 작업은 하루종일 걸릴 수 있어요.
카드가 많아봤자 얼마나 많겠냐, 그냥 이렇게 하면 안되냐구요?
맞아요. 카드가 1만개면 OK죠. 해봤자 1억 번 밖에 계산 안하니깐요.
하지만 카드가 만약 5억개라면요? 카드가 500억개라면?
과장된 숫자같지만, 데이터가 굉장히 많이 쌓이는 기업이라면 저 정도 데이터를 다뤄야할 수도 있어요.

여러분들이 작고, 간단한 프로그램만 평생 개발할 것이라면 이런 알고리즘적 사고를 공부하지 않아도 괜찮아요.
하지만 만약 여러분들이 알고리즘적 사고방식을 많이 공부한다면, 여러분들의 소프트웨어는 좀 더 멋지고, 끝내주게 될 거에요.

### 2. 중복 데이터 제거를 위해 Set 사용하기

Set, 한국어로는 집합! 이 친구는 아무리 우리가 중복된 데이터를 넣어도 중복하지 않게 저장해주는 친구에요.

뭔 말이냐면,

```
set.add('a');
set.add('a');
set.add('a');
set.add('a');

print(set) /// ['a']
```
이렇게 'a'를 수백만번 넣어도, set 안에 'a'는 1번만 들어가는 친구에요.

Set는 내부에서 알아서 중복체크를 하는데요, 그 속도는 N제곱에 비하면 무진장 빨라요. 어떻게 내부에 개발해놨느냐에 따라 다르지만, 대부분 O(1) 혹은 O(log(n))이에요. 무슨 말이냐면, 저장하려는 데이터 N이 아무리 커도 영향을 받지 않거나(O(1)), 혹은 log(N)만큼만 영향을 받는다는 이야기에요.
> log(N)이면 데이터가 1,000개면 10배 느려지고, 1,000,000개면 100배 느려져요.
> log에 밑이 없어 상용로그인 십진로그 log_10처럼 보이지만, 컴퓨터쪽에선 대부분 이진로그 log_2를 사용해요.

Set을 이용한다면 다음과 같이 코드를 짤 수 있을거에요.

```javascript
var set = new Set();
var 오른쪽카드더미 = 카드더미;

for (var 오른쪽카드 in 오른쪽카드더미) {
    set.add(오른쪽카드);
}

print('카드의 종류는 => ', set);
```

## 3. 데이터 중복 제외해서 내보내기

자, 그러면 지난번 getNeighborAndNeighborsOfNeighbor 코드에서 중복한 이웃을 제거하여 리턴해보도록 할게요.
아! 그리고 'user의 이웃과 이웃의 이웃'이니까, user는 들어가지 않는게 좋을 것 같아요. 결과물에서 혹시 user가 들어있으면 지워볼게요.

```cpp
std::vector<std::string> getNeighborAndNeighborsOfNeighbor(NeighborsMap& neighborsMap, std::string user) {
    // BEFORE => std::vector<std::string> result;
    std::set<std::string> result; 

    auto neighbors = getNeighbors(neighborsMap, user);
    // BEFORE => result.insert(result.end(), neighbors.begin(), neighbors.end());
    result.insert(neighbors.begin(), neighbors.end()); 
    
    for (auto& neighbor : neighbors) {
        auto neighborsOfNeighbors = getNeighbors(neighborsMap, neighbor);
        // BEFORE => result.insert(neighborsOfNeighbors.begin(), neighborsOfNeighbors.end());
        result.insert();
    }

    result.erase(user); // 혹시 이웃 리스트에 user가 있을 수도 있으니, user는 결과에서 지워줍시다.

    // BEFORE => return result;
    return std::vector<std::string>(result.begin(), result.end());
}
```

이렇게 완성!


# 정리해보기

Map, Dictionary 라는 친구는 Key에 해당하는 Value를 저장할 수 있는 저장소에요.
Key를 알고 있으면 Value를 한방에 가져올 수 있어요.

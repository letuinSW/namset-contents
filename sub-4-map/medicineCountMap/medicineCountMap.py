medicineCountMap = {} # 파이썬에선 {} 이게 비어있는 Dictionary에요.

while True:
    medicineName = input("약의 이름은? 입력할 거 없으면 '끝'을 입력해주세요: ")
    if medicineName == "끝":
        break
    if medicineCountMap.get(medicineName) == None:
        medicineCountMap[medicineName] = 1
    else:
        medicineCountMap[medicineName] += 1

medicineName = input("갯수가 궁금한 약의 이름은?: ")
medicineCount = medicineCountMap[medicineName]
print(f"약 {medicineName}의 갯수는 {medicineCount}개 입니다 땡큐.")

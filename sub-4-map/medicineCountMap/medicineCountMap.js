// javascript는 터미널(cmd)에서 입력받는게 너무 까다로우니, 그냥 코드에 값을 넣어서 해볼게요.

// 이런 입력이 한줄 씩 들어온다고 가정할게요.
const inputLines = [
    '아스피린',
    '아스피린',
    '아세트아미노펜',
    '덱시부프로펜',
    '아세트아미노펜',
    '이소트레티노인',
    '끝',
    '덱시부프로펜',
];

// 이 함수가 inputLines에서 하나씩 빼서 입력이라고 알려줄거에요.
function getInputLine() {
    return inputLines.shift();
}

const medicineCountMap = new Map(); // javascript도 파이썬과 똑같이 {}를 Map으로 사용할 수도 있어요. 엄연하겐 js의 {}는 Map이 아니라 Object지만요! new Map()을 {}로 바꿔도 동작하게 아래 코드를 짜긴 했어요. 바꿔보셔도 좋아요.
while (true) {
    const medicineName = getInputLine();
    console.log(`약의 이름은? 입력할 거 없으면 '끝'을 입력해주세요: ${medicineName}`);
    if (medicineName === '끝') {
        break;
    }
    if (!medicineCountMap[medicineName]) { // key에 해당하는 value가 없으면 undefined가 떠요. 그래서 앞에 !로 체크해요.
        medicineCountMap[medicineName] = 1;
    } else {
        medicineCountMap[medicineName] += 1;
    }
}

const medicineName = getInputLine();
console.log(`갯수가 궁금한 약의 이름은?: ${medicineName}`);
console.log(`약 ${medicineName}의 갯수는 ${medicineCountMap[medicineName]}개 입니다 땡큐`);
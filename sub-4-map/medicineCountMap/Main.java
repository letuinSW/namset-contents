import java.util.HashMap;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        var medicineCountMap = new HashMap<String, Integer>(); // Map은 인터페이스. HashMap은 'Hash'를 이용하여 만든 Map의 구현체!
                                                               // (공은 인터페이스, 고무공은 고무로 공을 만든 구현체! 랑 비슷함)

        try (var scanner = new Scanner(System.in)) {
            while (true) {
                System.out.print("약의 이름은? 입력할 거 없으면 '끝'을 입력해주세요: ");
                var medicineName = scanner.nextLine();
                if (medicineName.equals("끝")) {
                    break;
                }

                var count = medicineCountMap.get(medicineName);
                var nextCount = count == null ? 1 : count + 1; // 없었으면 1, 아니면 +1. 코드 간결하죠?
                medicineCountMap.put(medicineName, nextCount);
            }

            System.out.print("갯수가 궁금한 약의 이름은?: ");
            var medicineName = scanner.nextLine();
            var count = medicineCountMap.get(medicineName);
            System.out.println(String.format("약 %s의 갯수는 %d개 입니다 땡큐!", medicineName, count));
        }
    }
}

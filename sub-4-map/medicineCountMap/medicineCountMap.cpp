#include <map>
#include <string>
#include <iostream>

int main()
{
    std::map<std::string, int> medicineCountMap;
    std::string medicineName;

    while (true)
    {

        std::cout << "약의 이름은? 입력할 거 없으면 '끝'을 입력해주세요: ";
        std::cin >> medicineName;
        if (medicineName == "끝")
        {
            break;
        }

        if (medicineCountMap.find(medicineName) == medicineCountMap.end())
        {
            medicineCountMap[medicineName] = 1;
        }
        else
        {
            medicineCountMap[medicineName] += 1;
        }
    }

    std::cout << "갯수가 궁금한 약의 이름은?: ";
    std::cin >> medicineName;
    std::cout << "약 " << medicineName << "의 갯수는 " << medicineCountMap[medicineName] << "개 입니다 땡큐." << std::endl;

    return 0;
}

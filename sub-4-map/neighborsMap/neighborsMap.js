const fs = require('fs');

function addInNeighbors(neighborsMap, user, neighbor) {
    if (!neighborsMap[user]) {
        neighborsMap[user] = [];
    }
    neighborsMap[user].push(neighbor);
}    

function getNeighbors(neighborsMap, user) {
    return neighborsMap[user] || [];
}

function getNeighborAndNeighborsOfNeighbors(neighborsMap, user) {
    const result = new Set();

    const neighbors = getNeighbors(neighborsMap, user);
    neighbors.forEach(neighbor => {
        result.add(neighbor);

        const neighborsOfNeighbors = getNeighbors(neighborsMap, neighbor);

        neighborsOfNeighbors.forEach(x => result.add(x));
    });
        

    result.delete(user);

    return [...result]; // Array.from(result) 와 같아요.
}

const fileString = fs.readFileSync('./input.txt', 'utf-8');
const lines = fileString.split(/\r?\n/);

const neighborsMap = {};

const n = parseInt(lines[0]);

for (let i = 0; i < n; i += 1) {
    const [a, b] =lines[i + 1].split(' ');

    addInNeighbors(neighborsMap, a, b);
    addInNeighbors(neighborsMap, b, a);
}

const user = lines[lines.length - 1];
const neighborList = getNeighborAndNeighborsOfNeighbors(neighborsMap, user);

neighborList.forEach(neihgbor => {
    console.log(neihgbor);
});

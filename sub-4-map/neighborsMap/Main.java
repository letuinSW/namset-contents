import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class Main {
    private static void addInNeighbors(Map<String, List<String>> neighborsMap, String user, String neighbor) {
        if (!neighborsMap.containsKey(user)) {
            neighborsMap.put(user, new ArrayList<String>());
        }
        neighborsMap.get(user).add(neighbor);
    }

    private static Collection<String> getNeighbors(Map<String, List<String>> neighborsMap, String user) {
        if (!neighborsMap.containsKey(user)) {// user의 이웃이 없다면..?
            return Collections.emptyList(); // 빈 리스트 내보내기.
        }
        return neighborsMap.get(user);
    }

    private static Collection<String> getNeighborAndNeighborsOfNeighbors(Map<String, List<String>> neighborsMap,
            String user) {
        var result = new HashSet<String>();

        var neighbors = getNeighbors(neighborsMap, user);
        result.addAll(neighbors);

        for (var neighbor : neighbors) {
            var neighborsOfNeighbors = getNeighbors(neighborsMap, neighbor);
            result.addAll(neighborsOfNeighbors);
        }

        result.remove(user);

        return result;
    }

    public static void main(String[] args) {
        int n;
        String user;
        var neighborsMap = new HashMap<String, List<String>>();

        try (var scanner = new Scanner(System.in)) {
            n = scanner.nextInt();

            for (var i = 0; i < n; i += 1) {
                var a = scanner.next();
                var b = scanner.next();
                addInNeighbors(neighborsMap, a, b);
                addInNeighbors(neighborsMap, b, a);
            }

            user = scanner.next();
        }

        var neighborList = getNeighborAndNeighborsOfNeighbors(neighborsMap, user);
        for (var neighbor : neighborList) {
            System.out.println(neighbor);
        }
    }
}
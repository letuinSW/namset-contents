def addInNeighbors(neighborsMap, user, neighbor):
    if neighborsMap.get(user) is None:
        neighborsMap[user] = []
    neighborsMap[user].append(neighbor)

def getNeighbors(neighborsMap, user):
    if neighborsMap.get(user) is None:
        return []
    return neighborsMap[user]

def getNeighborAndNeighborsOfNeighbors(neighborsMap, user):
    result = set()

    neighbors = getNeighbors(neighborsMap, user)
    result.update(neighbors)
    
    for neighbor in neighbors:
        neighborsOfNeighbors = getNeighbors(neighborsMap, neighbor)
        result.update(neighborsOfNeighbors)

    result.remove(user)

    return list(result)


neighborsMap = {} # key: string, value: string[]


n = int(input())
for _ in range(n):
    a, b = input().split()
    addInNeighbors(neighborsMap, a, b)
    addInNeighbors(neighborsMap, b, a)


user = input()
neighborList = getNeighborAndNeighborsOfNeighbors(neighborsMap, user)

for neighbor in neighborList:
    print(neighbor)

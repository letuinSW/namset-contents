#include <iostream>
#include <map>
#include <string>
#include <vector>
#include <set>

using NeighborsMap = std::map<std::string, std::vector<std::string>>;

void addInNeighbors(NeighborsMap &neighborsMap, std::string user, std::string neighbor)
{
    if (neighborsMap.find(user) == neighborsMap.end())
    {
        neighborsMap[user] = std::vector<std::string>();
    }
    neighborsMap[user].push_back(neighbor);
}

std::vector<std::string> getNeighbors(NeighborsMap& neighborsMap, std::string user) {
    if (neighborsMap.find(user) == neighborsMap.end()) {
        // x의 이웃은 없는뎁쇼..?
        return std::vector<std::string>(); // 빈 리스트 내보내기.
    }
    return neighborsMap[user];
}

std::vector<std::string> getNeighborAndNeighborsOfNeighbors(NeighborsMap& neighborsMap, std::string user) {
    std::set<std::string> result;

    auto neighbors = getNeighbors(neighborsMap, user);
    result.insert(neighbors.begin(), neighbors.end());
    
    for (auto& neighbor : neighbors) {
        auto neighborsOfNeighbors = getNeighbors(neighborsMap, neighbor);
        result.insert(neighborsOfNeighbors.begin(), neighborsOfNeighbors.end());
    }

    result.erase(user);

    return std::vector<std::string>(result.begin(), result.end());
}

int main()
{
    NeighborsMap neighborsMap;
    int n;
    std::cin >> n;
    
    for (auto i = 0; i < n; i += 1) {
        std::string a, b;
        std::cin >> a >> b;
        addInNeighbors(neighborsMap, a, b);
        addInNeighbors(neighborsMap, b, a);
    }

    std::string user;
    std::cin >> user;
    
    auto neighborList = getNeighborAndNeighborsOfNeighbors(neighborsMap, user);
    for (auto& neighbor : neighborList) {
        std::cout << neighbor << "\n";
    }
    std::cout << std::endl;
    
    return 0;
}
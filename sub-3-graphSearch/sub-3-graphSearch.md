# 무엇을 연습해볼까?
여러분은 나무판자에 잉크를 한 방울 떨어뜨려본 적이 있는가?
나무판자에 잉크를 떨어뜨리면, 잉크가 나무결 사이 사이로 스며들어간다.

의성어로 표현하자면, "쏴~"하고 파고 들어간다.

이번엔 그런 걸 배워볼거다. 잉크를 똑 떨어뜨리면, 연결된 나무결 사이로 파고 들어가는 그런 것을.

# 1. 나로부터 5촌까지의 사람들은 누가 있는가?

어떻게 구하면 될까?
우선 나의 가족들을 계산한다.
그리고 그 가족의 가족을 계산해본다.
가족 of 가족의 가족을 계산해본다.
반복하면서, 5촌이 넘어가면 제외한다.
맞지? 혹시 나만 지구상에서 이렇게 5촌까지의 사람들을 탐색하는 것은 아니겠지? 여러분도 이게 맞다고 해줘...

이것을 코드로 나타낸다면 어떻게 될까?

우리는 누가 누구의 가족인지 알아야 한다.
쉽게 얘기하자면, 우리 할머니의 부모형제자매 관계가 어떻게 되는지 알아야한다.
특정한 사람 X의 가족이 누가 있는지 알아야한다.
어떻게 데이터를 저장해야 특정한 사람 X에 대한 가족 리스트를 가져올 수 있을까...?
저장하는 방법에 대해선 다음에 배워보도록 하고, 우선 "특정한 사람 X에 대한 가족 리스트를 가져오는 함수 가족(X)"가 있다고 하자.

가족(나)를 하면 나의 가족 사람들이 리스트로 나올것이다. 이해가 되는가?
가족(남세현) 은 [엄마, 아빠, 누나, 형]이다. 그럼 우선 합계가 4명이다.

그 다음엔 어떻게 해야하는가? 엄마, 아빠, 누나, 형의 가족들을 계산해야한다.

가족(엄마), 가족(아빠), 가족(누나), 가족(형) 을 합쳐야한다.

엄마의 가족엔 [외할머니, 외할아버지, 외삼촌, 이모1, 이모2, 아빠]가 나온다.
아빠의 가족엔 [할아버지, 할머니, 삼촌, 고모, 엄마]가 나온다.

어?
아빠랑 엄마가 또 나와버렸다. 엄마랑 아빠는 내가 이미 숫자를 셌는데..?
중복해서 세지 않도록 잘 제외 해야한다.

그 다음엔 엄마의 가족, 아빠의 가족에 가족(x) 함수를 써야한다.
근데 뭔가... 엄마 가족에 함수 쓰고, 아빠 가족에 함수 쓰는건 정리가 잘 안되는 것 같다.

차라리 이렇게 하면 어떨까?
"발견된 사람" 리스트를 하나 만들자.
"발견된 사람" 리스트에서 한명씩 뽑아서 가족(x)함수를 돌리자. 그러면 또 누가 나오겠지? 그 사람들을 다시 "발견된 사람"에 넣자.
혹시 중복되는 사람이 있을 수 있으니까, 중복을 체크하는 "이미 등록된 사람" 리스트도 만들자. 중복된 사람은 "발견된 사람"에 안들어가게 하자.
또, 5촌 초과면 계산하면 안되니까, 나랑 그 사람의 촌수를 계산해주는 함수로 잘 걸러내자.

코드로 짜면 어떻게 될까?
```
발견된 사람들 = 내 가족;
이미 등록된 사람들 = 내 가족;

while (발견된 사람들.수 > 0) {
    사람 = 발견된 사람들.한명 빼내기();
    그 사람의 가족들 = 가족(사람);

    for (가족 in 그 사람의 가족들) {
        if (가족 in 이미 등록된 사람들) {
            패스~ continue;
        }
        if (촌수(가족, 나) > 5) {
            패스~ continue;
        }
        발견된 사람.추가(가족)
        이미 등록된 사람.추가(가족)
    }
}
print("5촌 이내의 사람 수는 {이미 등록된 사람.수}명!")
```

어때, 감이 잘 오는가? 잘 모르겠으면 카톡으로 넘어가보자.

# 2. 내 카톡 친구, 친구의 친구, 친구의 친구의 친구.....의 수를 전부 더하면 몇명일까?

이건 어떻게 구하면 될까?

- 앞으로 탐색할 친구 리스트
- 이미 탐색된 친구 리스트
이 두가지면 끝났다. 나머지는 똑같다.

1. 내 카톡 친구들을 '앞으로 탐색할 친구 리스트'와 '이미 탐색된 친구 리스트'에 넣는다.
2. '앞으로 탐색할 친구 리스트'가 빌 때 까지, 아래 3~5를 반복한다.
3. '앞으로 탐색할 친구 리스트'에서 1명을 빼낸다. 그 사람을 '친구'라고 부르자.
4. '친구'의 친구를 구한다. 그 친구들을 '친구의 친구들'이라고 부르자.
5. '친구의 친구들'에서 이미 탐색된 친구를 제외하자. 그 다음 '앞으로 탐색할 친구 리스트'와 '이미 탐색된 친구 리스트'에 추가하자.

끝! 코드로 하자면 다음과 같겠다.
이번엔 변수명을 영어로 해보겠다. javascript의 문법을 사용할 것인데, 그렇게 어렵지 않을 것이다. 최대한 쉽게 해볼게.

```js
const myFriends = getFriends(me);
const searchingFriends = [];
const searchedFriends = [];
searchingFriends.push(myFriends);
searchedFriends.push(myFriends);

while (searchingFriends.length > 0) {
    const friend = searchingFriends.pop();
    const friendsOfFriend = getFriends(friend);

    for (const friendOfFriend of friendsOfFriend) {
        if (searchedFriends.includes(friendOfFriend)) {
            continue;
        }
        searchedFriends.push(friendOfFriend);
        searchingFriends.push(friendOfFriend);
    }
}
console.log(`연결된 친구 수 : ${searchedFriends.soze}`);
```

"이미 검색한 친구 리스트"가 만약 배열이라면, 내가 어떤 사람 Y를 이미 검색했는지 아닌지 알아내는데 얼마나 걸릴까?
배열의 모든 요소에 일일이 비교해야하기 때문에 배열의 크기만큼 (O(n)) 시간이 걸릴 것이다. 
문제가 될 수 있다. 프로그램이 너무 느려질 수 있다.

뭔 말이냐면, 당신 생각엔 위의 while이 몇번 돌 것이라 생각하는가? 총 연결된 친구 수만큼 while이 돌 것이다.
또한 "이미 검색한 친구 리스트"의 크기도 총 연결된 친구 수만큼 늘어날 것이다.

while도 연결된 친구 수 만큼 돌고, 이미 검색한 친구인지 아닌지 확인하는 것도 연결된 친구 수 만큼 계산이 되어야한다?
이것은 곧, n x n 번만큼 걸린다는 것을 의미한다 (O(n^2))
친구가 10명이면 별로 상관 없는데, 만약 친구가 100,000명이라면? 순식간에 연산 수가 10억, 100억을 넘어버릴 수 있다.

이미 검색한 친구인지 아닌지 좀 더 빠르게 알 수 있는 방법은 없을까?
특정 요소가 이미 있는지 아닌지 바로 알 수 있는 (O(1)) Set이라는 자료구조가 있다.
그 중 HashSet이라는 친구가 있는데, 지금은 이런게 있다~ 하고 넘어가자. 나중에 다시 설명해주겠다.

Set을 이용하면 다음과 같이 코드를 짤 수 있을 것 같다.

```js
const myFriends = getFriends(me);
const searchingFriends = [];
searchingFriends.push(myFriends);
const searchedFriends = new Set(myFriends);

while (searchingFriends.length > 0) {
    const friend = searchingFriends.pop();
    const friendsOfFriend = getFriends(friend);

    for (const friendOfFriend of friendsOfFriend) {
        if (searchedFriends.has(friendOfFriend)) {
            continue;
        }
        searchedFriends.add(friendOfFriend);
        searchingFriends.push(friendOfFriend);
    }
}
console.log(`연결된 친구 수 : ${searchedFriends.soze}`);
```

# 정리하면서 패턴 익히기

나는 뭐든지 패턴을 익히는게 중요하다고 생각한다. 이번에 배운 것의 패턴을 보자.

1. 시작점 X와 연결된 것들을 가져온다.
2. 그것을 '앞으로 검색할 리스트'에 넣는다.
3. '앞으로 검색할 리스트'에서 하나씩 빼낸다.
4. 빼낸 녀석과 연결된 것을 가져온다.
5. 걔가 이제까지 나온 적이 없는지, 아니면 뭐 다른 조건에 걸리는게 없는지 확인한다(우리 문제로 치면 '잘린 나뭇가지'인지 확인한다거나)
6. 결격 사유가 없다면 그 녀석을 '앞으로 검색할 리스트'에 추가한다.

쉽다! 그치? 쉬워!
참고로, 리스트에서 앞에서 부터 빼면 dfs, 뒤에서부터 빼면 bfs다.
뭔소리냐고? 나중에 정통적인 알고리즘 수업을 듣게 된다면 알게 될 것이다. 엄청 특별한 것은 아니니 한번 보시길 추천.

hits = 0
idSet = set()

while True:
    id = input()
    if id == "#END#":
        break
    hits += 1
    idSet.add(id)

print(f"{hits} {len(idSet)}")

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        var hits = 0;
        var idSet = new HashSet<String>();

        try (var scanner = new Scanner(System.in)) {
            while (true) {
                var id = scanner.next();

                if (id.equals("#END#")) {
                    break;
                }
                    
                hits += 1;
                idSet.add(id);
            }
        }

        System.out.println(String.format("%d %d", hits, idSet.size()));
    }
}
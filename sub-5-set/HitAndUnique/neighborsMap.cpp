#include <iostream>
#include <map>
#include <string>
#include <set>

int main()
{
    int hits = 0;
    std::set<std::string> idSet;

    while (true) {
        std::string id;
        std::cin >> id;
        
        if (id == "#END#") {
            break;
        }
            
        hits += 1;
        idSet.insert(id);
    }
        
    std::cout << hits << " " << idSet.size() << std::endl;
    return 0;
}

const fs = require('fs');

const fileString = fs.readFileSync('./input.txt', 'utf-8');
const lines = fileString.split(/\r?\n/);

let hits = 0;
const idSet = new Set();

for (var i = 0; ; i += 1) {
    id = lines[i];
    if (id == "#END#") {
        break;
    }
    hits += 1
    idSet.add(id)
}

console.log(`${hits} ${idSet.size}`);
